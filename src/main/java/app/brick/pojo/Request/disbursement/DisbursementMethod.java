package app.brick.pojo.Request.disbursement;

public class DisbursementMethod {

    public DisbursementMethod() {
    }

    public DisbursementMethod(String bankAccountHolderName, String bankAccountNo, String bankShortCode) {
        this.bankAccountHolderName = bankAccountHolderName;
        this.bankAccountNo = bankAccountNo;
        this.bankShortCode = bankShortCode;
    }

    private String bankShortCode;

    private String bankAccountHolderName;

    private String bankAccountNo;

    private final String type = "bank_transfer";


    @Override
    public String toString() {
        return "ClassPojo [bankShortCode = " + bankShortCode + ", bankAccountHolderName = " + bankAccountHolderName + ", bankAccountNo = " + bankAccountNo + ", type = " + type + "]";
    }

    public String getBankShortCode() {
        return bankShortCode;
    }

    public String getBankAccountHolderName() {
        return bankAccountHolderName;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public String getType() {
        return type;
    }
}
