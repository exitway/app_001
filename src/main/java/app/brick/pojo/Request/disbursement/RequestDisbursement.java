package app.brick.pojo.Request.disbursement;

public class RequestDisbursement {

    private String amount;

    private String referenceId;

    private String description;

    private DisbursementMethod disbursementMethod;

    public RequestDisbursement(String amount, String referenceId, String description, DisbursementMethod disbursementMethod) {
        this.amount = amount;
        this.referenceId = referenceId;
        this.description = description;
        this.disbursementMethod = disbursementMethod;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [amount = "+amount+", description = "+description+", disbursementMethod = "+disbursementMethod+", referenceId = "+referenceId+"]";
    }
}
