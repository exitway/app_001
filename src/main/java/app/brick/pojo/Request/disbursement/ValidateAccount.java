package app.brick.pojo.Request.disbursement;

public class ValidateAccount {
    private String bankShortCode;

    private String accountNumber;

    public ValidateAccount(String accountNumber, String bankShortCode) {
        this.accountNumber = accountNumber;
        this.bankShortCode = bankShortCode;
    }

    public String getBankShortCode ()
    {
        return bankShortCode;
    }

    public void setBankShortCode (String bankShortCode)
    {
        this.bankShortCode = bankShortCode;
    }

    public String getAccountNumber ()
    {
        return accountNumber;
    }

    public void setAccountNumber (String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [bankShortCode = "+bankShortCode+", accountNumber = "+accountNumber+"]";
    }
}
