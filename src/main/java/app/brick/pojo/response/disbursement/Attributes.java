package app.brick.pojo.response.disbursement;

public class Attributes {
    private String createdAt;

    private String amount;

    private String description;

    private DisbursementMethod disbursementMethod;

    private String referenceId;

    private String status;

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public DisbursementMethod getDisbursementMethod ()
    {
        return disbursementMethod;
    }

    public void setDisbursementMethod (DisbursementMethod disbursementMethod)
    {
        this.disbursementMethod = disbursementMethod;
    }

    public String getReferenceId ()
    {
        return referenceId;
    }

    public void setReferenceId (String referenceId)
    {
        this.referenceId = referenceId;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [createdAt = "+createdAt+", amount = "+amount+", description = "+description+", disbursementMethod = "+disbursementMethod+", referenceId = "+referenceId+", status = "+status+"]";
    }
}
