package app.brick.pojo.response.disbursement;

public class DisbursementMethod {
    private String bankShortCode;

    private String bankAccountHolderName;

    private String bankAccountNo;

    private String serverBankAccountHolderName;

    private String bankName;

    private String type;

    public String getBankShortCode ()
    {
        return bankShortCode;
    }

    public void setBankShortCode (String bankShortCode)
    {
        this.bankShortCode = bankShortCode;
    }

    public String getBankAccountHolderName ()
    {
        return bankAccountHolderName;
    }

    public void setBankAccountHolderName (String bankAccountHolderName)
    {
        this.bankAccountHolderName = bankAccountHolderName;
    }

    public String getBankAccountNo ()
    {
        return bankAccountNo;
    }

    public void setBankAccountNo (String bankAccountNo)
    {
        this.bankAccountNo = bankAccountNo;
    }

    public String getServerBankAccountHolderName ()
    {
        return serverBankAccountHolderName;
    }

    public void setServerBankAccountHolderName (String serverBankAccountHolderName)
    {
        this.serverBankAccountHolderName = serverBankAccountHolderName;
    }

    public String getBankName ()
    {
        return bankName;
    }

    public void setBankName (String bankName)
    {
        this.bankName = bankName;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [bankShortCode = "+bankShortCode+", bankAccountHolderName = "+bankAccountHolderName+", bankAccountNo = "+bankAccountNo+", serverBankAccountHolderName = "+serverBankAccountHolderName+", bankName = "+bankName+", type = "+type+"]";
    }
}
