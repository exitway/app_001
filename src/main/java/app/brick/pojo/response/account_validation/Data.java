package app.brick.pojo.response.account_validation;

public class Data {
    private String accountName;

    private String bankShortCode;

    private String accountNo;

    public String getAccountName ()
    {
        return accountName;
    }

    public void setAccountName (String accountName)
    {
        this.accountName = accountName;
    }

    public String getBankShortCode ()
    {
        return bankShortCode;
    }

    public void setBankShortCode (String bankShortCode)
    {
        this.bankShortCode = bankShortCode;
    }

    public String getAccountNo ()
    {
        return accountNo;
    }

    public void setAccountNo (String accountNo)
    {
        this.accountNo = accountNo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [accountName = "+accountName+", bankShortCode = "+bankShortCode+", accountNo = "+accountNo+"]";
    }
}
