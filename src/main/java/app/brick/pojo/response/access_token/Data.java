package app.brick.pojo.response.access_token;

public class Data {
    private String access_token;

    private String primary_color;

    public String getAccess_token ()
    {
        return access_token;
    }

    public void setAccess_token (String access_token)
    {
        this.access_token = access_token;
    }

    public String getPrimary_color ()
    {
        return primary_color;
    }

    public void setPrimary_color (String primary_color)
    {
        this.primary_color = primary_color;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [access_token = "+access_token+", primary_color = "+primary_color+"]";
    }
}
