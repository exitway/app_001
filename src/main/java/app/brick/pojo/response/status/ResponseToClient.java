package app.brick.pojo.response.status;

public class ResponseToClient {
    String Status;
    String Message;

    public String getStatus() {
        return Status;
    }

    public String getMessage() {
        return Message;
    }

    public ResponseToClient(String status, String message) {
        Status = status;
        Message = message;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
