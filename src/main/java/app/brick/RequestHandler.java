package app.brick;

import app.brick.model.Transactions;
import app.brick.pojo.Request.disbursement.ValidateAccount;
import app.brick.pojo.Request.disbursement.DisbursementMethod;
import app.brick.pojo.Request.disbursement.RequestDisbursement;
import app.brick.pojo.response.access_token.AccessToken;
import app.brick.pojo.response.account_validation.AccountValidation;
import app.brick.pojo.response.disbursement.Disbursement;
import app.brick.pojo.response.status.ResponseToClient;
import app.brick.utility.TransactionDB;
import com.google.gson.Gson;
import okhttp3.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Base64;

@RestController
public class RequestHandler {

    public static final String INVALID = "invalid";
    private final String API_BASE_ONE_BRICK = "https://sandbox.onebrick.io/v1";
    private final String API_AUTH_TOKEN = API_BASE_ONE_BRICK + "/auth/token";
    private final String API_VALIDATE_ACCOUNT = API_BASE_ONE_BRICK + "/payments/bank-account-validation";
    private final String API_PAYMENT = API_BASE_ONE_BRICK + "/payments/disbursements";

    public static final String COMPLETED = "Completed";
    public static final String FAILED = "Failed";
    public static final String SUCCEED = "Succeed";
    public static final String INVALID_ACCOUNT = "Invalid Account";
    public static final String GENERAL_ERROR = "General Error";
    public static final String NO_CONTRACT_ID = "Contract ID Missing For Current Transaction";

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RequestHandler.class);
    final Gson gson = new Gson();

    private final OkHttpClient client = new OkHttpClient();
    private String publicAccessToken;
    private String encodedClientCredential = Base64.getEncoder().encodeToString((Properties.clientId + ":" + Properties.clientSecret).getBytes());

    @GetMapping("/cek_norek/{accountNumber}/{bankShortCode}")
    public String validateAccount(
            @PathVariable String accountNumber,
            @PathVariable String bankShortCode)
            throws IOException {

        ResponseToClient response = new ResponseToClient(FAILED, GENERAL_ERROR);

        String responseString = getValidateAccount(accountNumber, bankShortCode);
        AccountValidation validity = gson.fromJson(responseString, AccountValidation.class);

        if (!validity.getData().getAccountName().equalsIgnoreCase(INVALID)) {
            response.setStatus(SUCCEED);
            response.setMessage(gson.toJson(validity.getData()));
        } else
            response.setMessage(INVALID_ACCOUNT);

        return gson.toJson(response).replaceAll("\\\\" ,"");
    }

    private String getValidateAccount(String accountNumber, String bankShortCode) {
        String result = "";
        try {
            result = requestAccountValidation(accountNumber, bankShortCode);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return result;
    }

    @GetMapping("/{accountNumber}/{bankShortCode}/{amount}/{refId}/{trxId}")
    public String transfer(
            @PathVariable String accountNumber,
            @PathVariable String bankShortCode,
            @PathVariable String amount,
            @PathVariable String refId,
            @PathVariable String trxId)
            throws IOException {

        ResponseToClient response = new ResponseToClient("Failed", "General Error");
        String responseToClientString = gson.toJson(response);
        Transactions trx = new Transactions();

        try {
            if (isRequestValid(accountNumber, amount, trxId)) {
                if (isTransactionExist(trxId)) {
                    logger.info("Transaction Exists. Checking ContractId");
                    Transactions currentTrxLog = getCurrentTransactionLog(trxId);
                    if (isContractIdExists(currentTrxLog.getResponse())) {
                        logger.info("ContracId Exists. Checking Status");
                        if (isStatusFinal(currentTrxLog.getResponse())) {
                            logger.info("Status Final");
                            ResponseToClient existedResponse = gson.fromJson(currentTrxLog.getResponseToClient(), ResponseToClient.class);
                            response.setMessage(existedResponse.getMessage());
                            response.setStatus(existedResponse.getStatus());
                        } else {
                            logger.info("Updating Current Transaction status");
                            String retryResponse = retryTransaction(currentTrxLog.getResponse());
                            if (!retryResponse.equalsIgnoreCase("")) {
                                logger.info("Retry Response Json");
                                Disbursement disbursement = gson.fromJson(retryResponse, Disbursement.class);
                                response.setMessage(gson.toJson(disbursement.getData().getAttributes()));
                                if (disbursement.getData().getAttributes().getStatus().equalsIgnoreCase("completed")) {
                                    response.setStatus(SUCCEED);
                                    TransactionDB.updateTransactionStatusByTrxid(retryResponse, gson.toJson(response), trxId);
                                }
                            }else{
                                response.setMessage("Error Updating Transaction Status");
                            }
                        }
                    } else {
                        response.setMessage(NO_CONTRACT_ID);
                    }
                } else {
                    String responseString = getValidateAccount(accountNumber, bankShortCode);
                    AccountValidation validity = gson.fromJson(responseString, AccountValidation.class);
                    if (!validity.getData().getAccountName().equalsIgnoreCase(INVALID)) {
                        DisbursementMethod disbursementMethod = getAccountDetails(accountNumber, bankShortCode);
                        String disbursementResponse = getTransferResult(amount, refId, disbursementMethod);
                        if (!disbursementResponse.equalsIgnoreCase("")) {
                            Disbursement disbursement = gson.fromJson(disbursementResponse, Disbursement.class);
                            response.setMessage(gson.toJson(disbursement.getData().getAttributes()));
                            if (disbursement.getData().getAttributes().getStatus().equalsIgnoreCase("completed")) {
                                response.setStatus(SUCCEED);
                            }
                            Transactions newTrxLog = new Transactions(
                                    trxId, "/"+accountNumber+"/"+bankShortCode+"/"+amount+"/"+refId+"/"+trxId+"/", gson.toJson(disbursementResponse), gson.toJson(response));
                            TransactionDB.saveTransaction(trx);
                        } else {
                            response.setMessage("Failed Requesting Disbursement");
                        }
                    } else {
                        response.setMessage("Account Invalid");
                    }
                }
            } else {
                response.setMessage("Input Invalid");
            }
        } catch (Exception e) {
            e.printStackTrace();;
            logger.error(e.getMessage());
        }

        return gson.toJson(response).replaceAll("\\\\", "");
    }


    private boolean isRequestValid(String accountNumber, String amount, String trxId) {
        return (isNumeric(accountNumber) && isNumeric(amount) && isNumeric(trxId));
    }

    private boolean isNumeric(String input) {
        boolean result = false;
        try {
            Integer.parseInt(input);
            result = true;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return result;
    }

    private String getTransferResult(String amount, String refId, DisbursementMethod disbursementMethod) {

        RequestDisbursement requestDisbursement = new RequestDisbursement(amount, refId, "", disbursementMethod);

        String jsonRequestDisbursement = gson.toJson(requestDisbursement);
        String resultString = createDisbursement(jsonRequestDisbursement);

        /*Disbursement disbursement = gson.fromJson(resultString, Disbursement.class);*/

        return resultString;
    }

    private DisbursementMethod getAccountDetails(String accountNumber, String bankShortCode) {
        DisbursementMethod disbursementMethod = new DisbursementMethod();
        try {
            disbursementMethod = createDisbursementMethod(accountNumber, bankShortCode);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return disbursementMethod;
    }

    private DisbursementMethod createDisbursementMethod(String accountNumber, String bankShortCode) throws Exception {
        String accountDetails = getValidateAccount(accountNumber, bankShortCode);

        AccountValidation accountValidation = gson.fromJson(accountDetails, AccountValidation.class);
        return new DisbursementMethod(
                accountValidation.getData().getAccountName(),
                accountValidation.getData().getAccountNo(),
                accountValidation.getData().getBankShortCode());
    }

    private boolean isAccountValid(DisbursementMethod disbursementMethod) {
        return disbursementMethod.getBankAccountHolderName().equalsIgnoreCase(INVALID);
    }

    private String createDisbursement(String requestDisbursement) {
        String result = "";
        try {
            result = requestApiMakeDisbursement(requestDisbursement);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return result;
    }

    private String requestAccountValidation(String accountNumber, String bankShortCode) throws Exception {
        logger.info("Validating Account");
        MediaType mediaType = MediaType.parse("application/json");
        ValidateAccount validateAccount = new ValidateAccount(accountNumber, bankShortCode);
        RequestBody body = RequestBody.create(gson.toJson(validateAccount), mediaType);
        Request request = new Request.Builder()
                .url(Properties.validateAccountUrl)
                .method("POST", body)
                .addHeader("Authorization", "Bearer " + getPublicAccessToken())
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();

        String responseString = response.body().string();
        logger.info("Account Details = " + responseString);

        return responseString;
    }

    private String requestApiMakeDisbursement(String requestDisbursement) throws Exception {

        logger.info("Requesting Disbursement");
        String jsonString = requestDisbursement;

        /**/
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(jsonString, mediaType);

//        logger.info("BODY : "+requestDisbursement);

        Request request = new Request.Builder()
                .url(Properties.disbursementUrl)
                .method("POST", body)
                .addHeader("Authorization", "Bearer " + getPublicAccessToken())
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();
        /**/

        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String responseString = response.body().string();
        logger.info(responseString);

        return responseString;
    }


    private String getPublicAccessToken() throws Exception {

        logger.info("Getting Public Access Token");

        Request request = new Request.Builder()
                .url(API_AUTH_TOKEN)
                .addHeader("Authorization", "Bearer " + encodedClientCredential)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new Exception("Failed Get Access Token " + response);

        String responseString = response.body().string();
        logger.info("Response String = " + responseString);

        AccessToken accessToken = gson.fromJson(responseString, AccessToken.class);

        return accessToken.getData().getAccess_token();
    }

    private boolean isContractIdExists(String response) {
        Disbursement disbursement = gson.fromJson(response, Disbursement.class);
        return disbursement.getData().getId() != null;
    }

    private String retryTransaction(String currentTrxResponse) {
        Disbursement disbursement = gson.fromJson(currentTrxResponse, Disbursement.class);
        String contractId = disbursement.getData().getId();
        String response = "";
        try {
            response = requestApiGetTransaction(contractId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private String requestApiGetTransaction(String contractId) throws Exception {
        logger.info("Requesting Disbursement Data");

        Request request = new Request.Builder()
                .url(Properties.disbursementUrl + "/" + contractId)
                .addHeader("Authorization", "Bearer " + getPublicAccessToken())
                .addHeader("Content-Type", "application/json")
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String responseString = response.body().string();
        logger.info("Response String " + responseString);

        return responseString;
    }

    private Transactions getCurrentTransactionLog(String trxId) {
        Transactions trx = new Transactions();
        try {
            trx = TransactionDB.getTransactionByTrxId(trxId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return trx;
    }

    private boolean isStatusFinal(String responseString) {
        boolean result = false;
        try {
            result = getTransactionStatus(responseString);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return result;
    }

    private boolean getTransactionStatus(String jsonString) throws Exception {
        Disbursement disbursementResponse = gson.fromJson(jsonString, Disbursement.class);
        logger.info("CURRENT TRANSACTION STATUS PARSING >>>>");
        logger.info(disbursementResponse.getData().getId());
        logger.info(disbursementResponse.getData().getAttributes().getStatus());
        return disbursementResponse.getData().getAttributes().getStatus().equalsIgnoreCase(COMPLETED) ||
                disbursementResponse.getData().getAttributes().getStatus().equalsIgnoreCase(FAILED);
    }

    private static boolean isTransactionExist(String trxId) {
        return TransactionDB.isTransactionExist(trxId);
    }


}
