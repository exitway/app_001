package app.brick.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Callback {
    private int id;
    private String requestId;
    private String response;
    private Timestamp responseDate;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "request_id", nullable = false, length = 50)
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "response", nullable = false, length = 1000)
    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Basic
    @Column(name = "response_date", nullable = false)
    public Timestamp getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Timestamp responseDate) {
        this.responseDate = responseDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Callback callback = (Callback) o;
        return id == callback.id &&
                Objects.equals(requestId, callback.requestId) &&
                Objects.equals(response, callback.response) &&
                Objects.equals(responseDate, callback.responseDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, requestId, response, responseDate);
    }
}
