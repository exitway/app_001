package app.brick.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;

@Entity
public class Transactions {

    private int id;
    private String trxId;
    private String request;
    private String response;
    private String responseToClient;
    private Timestamp createdAt;

    public Transactions() {
    }

    public Transactions(String trxId, String request, String response, String responseToClient) {
        Instant instant = Instant.now();
        Timestamp sqlTimestamp = Timestamp.from(instant);

        this.trxId = trxId;
        this.request = request;
        this.response = response;
        this.responseToClient = responseToClient;
        this.createdAt = sqlTimestamp;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "trx_id", nullable = false, length = 64)
    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    @Basic
    @Column(name = "request", nullable = false, length = -1)
    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    @Basic
    @Column(name = "response", nullable = false, length = -1)
    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Basic
    @Column(name = "response_to_client", nullable = false, length = -1)
    public String getResponseToClient() {
        return responseToClient;
    }

    public void setResponseToClient(String responseToClient) {
        this.responseToClient = responseToClient;
    }

    @Basic
    @Column(name = "created_at", nullable = false)
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transactions that = (Transactions) o;
        return id == that.id &&
                Objects.equals(trxId, that.trxId) &&
                Objects.equals(request, that.request) &&
                Objects.equals(response, that.response) &&
                Objects.equals(responseToClient, that.responseToClient) &&
                Objects.equals(createdAt, that.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, trxId, request, response, responseToClient, createdAt);
    }
}
