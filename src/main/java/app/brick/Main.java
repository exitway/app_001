package app.brick;

import app.brick.model.Transactions;
import app.brick.pojo.response.disbursement.Disbursement;
import app.brick.pojo.response.status.ResponseToClient;
import app.brick.utility.TransactionDB;
import com.google.gson.Gson;
import okhttp3.*;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Calendar;

public class Main {
    public static final String COMPLETED = "Completed";
    public static final String FAILED = "Failed";
    public static final String GENERAL_ERROR = "General Error";
    public static final String NO_CONTRACT_ID = "No Contract ID For Current Transaction";
    static Gson gson = new Gson();
    private static final OkHttpClient client = new OkHttpClient();

    public static void main(String[] args) {

        String trxId = "333";
        ResponseToClient response = new ResponseToClient(FAILED, GENERAL_ERROR);
        String responseStringToClient = gson.toJson(response);

//        String
        if (isTransactionExist(trxId)) {
            Transactions currentTrxLog = getCurrentTransactionLog(trxId);
            if (isContractIdExists(currentTrxLog.getResponse())) {
                if (isStatusFinal(currentTrxLog.getResponse()))
                    responseStringToClient = currentTrxLog.getResponseToClient();
                else {
                    System.out.println("Retrying Current Transaction");
                    String retryResponse = retryTransaction(currentTrxLog.getResponse());
                }
            } else {
                response.setMessage(NO_CONTRACT_ID);
            }

        }

        System.out.println(responseStringToClient);


//        responseStringToClient = gson.toJson();

//        System.out.println("FINAL RESULT >>> : " + responseStringToClient);

        Transactions trx = new Transactions(trxId, "", "", "");
        Integer result = TransactionDB.saveTransaction(trx);
//        System.out.println("RESULT >>>> " +result);

        /*Transactions transactions = gson.fromJson(String.valueOf(TransactionDB.getTransactionByTrxId("333").get(0)), Transactions.class);
        System.out.println("Update Result = " +TransactionDB.updateTransactionStatusByTrxYd("","3335"));*/
    }


    private static Timestamp getTimestamp() {
        long time = Calendar.getInstance().getTime().getTime();
        return new Timestamp(time);
    }

    private static boolean isContractIdExists(String response) {
        Disbursement disbursement = gson.fromJson(response, Disbursement.class);
        return disbursement.getData().getId() != null;
    }

    private static String retryTransaction(String currentTrxResponse) {
        Disbursement disbursement = gson.fromJson(currentTrxResponse, Disbursement.class);
        String contractId =  disbursement.getData().getId();
        String response =  "";
        try {
           response = requestApiGetTransaction(contractId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private static String requestApiGetTransaction(String contractId) throws Exception{
        System.out.println("Requesting Disbursement Data for ID : "+contractId);

        Request request = new Request.Builder()
                .url(Properties.disbursementUrl+"/"+contractId)
                .addHeader("Authorization", "Bearer " + "<<<< TOKEN >>>>")
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String responseString = response.body().string();
        System.out.println(responseString);

        return responseString;
    }

    private static Transactions getCurrentTransactionLog(String trxId) {
        Transactions trx = new Transactions();
        try {
            trx = TransactionDB.getTransactionByTrxId(trxId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return trx;
    }

//    private static String getResponseToClient(String trxId) {
//        try{
//
//        }catch (Exception e){
//
//        }
//    }

    private static boolean isStatusFinal(String responseString) {
        boolean result = false;
        try {
            result = getTransactionStatus(responseString);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return result;
    }

    private static boolean getTransactionStatus(String jsonString) throws Exception {
        Disbursement disbursementResponse = gson.fromJson(jsonString, Disbursement.class);
        System.out.println("CURRENT TRANSACTION STATUS PARSING >>>>");
        System.out.println(disbursementResponse.getData().getId());
        System.out.println(disbursementResponse.getData().getAttributes().getStatus());
        return disbursementResponse.getData().getAttributes().getStatus().equalsIgnoreCase(COMPLETED) ||
                disbursementResponse.getData().getAttributes().getStatus().equalsIgnoreCase(FAILED);
    }

    private static boolean isTransactionExist(String trxId) {
        return TransactionDB.isTransactionExist(trxId);
    }

}
