package app.brick.utility;

import app.brick.RequestHandler;
import app.brick.model.Transactions;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class TransactionDB {
    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RequestHandler.class);

    public static Transactions getTransactionByTrxId(String trxId) {
        Session session = DatabaseUtil.getSessionFactory().openSession();
        List<Transactions> transactionsEntities;
        Transactions trx = new Transactions();

        try {
            Query q = session.createQuery("from Transactions  where trxId like :trx_id");
            q.setParameter("trx_id", trxId);
            q.setMaxResults(1);
            transactionsEntities = q.list();
            trx = transactionsEntities.get(0);

        } catch (HibernateException e) {
            logger.error(e.getMessage());
        } finally {
            session.close();
        }
        return trx;
    }

    public static boolean isTransactionExist(String trxId) {
        Session session = DatabaseUtil.getSessionFactory().openSession();
        boolean result = false;

        try {
            Query q = session.createQuery("select id from Transactions where trxId like :trx_id");
            q.setParameter("trx_id", trxId);
            q.setMaxResults(1);
            if (!q.list().isEmpty())
                result = true;
            else
                logger.info("Result Query For " + trxId + " Return Empty Row");
        } catch (HibernateException e) {
            logger.error(e.getMessage());
        } finally {
            session.close();
        }
        return result;
    }

    public static boolean updateTransactionStatusByTrxid(String response, String respToClient, String trxId) {
        Session session = DatabaseUtil.getSessionFactory().openSession();
        int resultRow = 0;
        Transaction trx = session.beginTransaction();
        try {
            Query q = session.createQuery("update Transactions set response=:response, responseToClient=:respToClient where trxId=:trxId");
            q.setParameter("response", response);
            q.setParameter("respToClient", respToClient);
            q.setParameter("trxId", trxId);
            resultRow = q.executeUpdate();
            trx.commit();
        } catch (HibernateException e) {
            trx.rollback();
            logger.error(e.getMessage());
        } finally {
            session.close();
        }

        return resultRow > 0;
    }

    public static Integer saveTransaction(Transactions transaction) {
        Session session = DatabaseUtil.getSessionFactory().openSession();
        Integer id = 0;
        Transactions newLog = transaction;
        Transaction trx = session.beginTransaction();
        try {
            session.save(newLog);
            trx.commit();
            id=newLog.getId();
        } catch (HibernateException e) {
            trx.rollback();
            e.printStackTrace();
            logger.error(e.getMessage());
        } finally {
            session.close();
        }

        logger.info("GENERATED ID : "+newLog.getId());

        return newLog.getId();
    }

    public static List<Transactions> contract_id() {
        Session session = DatabaseUtil.getSessionFactory().openSession();
        List<Transactions> transactionsEntities = null;

        try {
//            final String sql = "from Transaction where ";
//            Query q = session.createQuery("from Transactions  where trxId=:id");
            Query q = session.createQuery("from Transactions  where response like :contractId");
            q.setParameter("contractId", "%contract_b4c6a877d2f24c4c9176b92ad4964d46%");
            transactionsEntities = q.list();

        } catch (HibernateException e) {
            logger.error(e.getMessage());
        } finally {
            session.close();
        }

        return transactionsEntities;
    }


}
