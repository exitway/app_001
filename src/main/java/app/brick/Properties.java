package app.brick;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Properties {

    public static String clientId;
    public static String clientSecret;
    public static String validateAccountUrl;
    public static String disbursementUrl;

    @Value("${client.id}")
    public void setClientId(String value) {
        this.clientId = value;
    }

    @Value("${client.secret}")
    public void setClientSecret(String value) {
        this.clientSecret = value;
    }

    @Value("${url.validate}")
    public void setValidateAccountUrl(String value) {
        this.validateAccountUrl = value;
    }

    @Value("${url.disbursement}")
    public void setDisbursementUrl(String value) {
        this.disbursementUrl = value;
    }

    @Autowired
    public Properties() {
    }


}
